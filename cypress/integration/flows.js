/// <reference types="Cypress" />

it('Ko flow', () => {

  cy.visit('');
  cy.get('.sc-kDDrLX').click();
  cy.get('.HmiKf').click();
  cy.get('#password').type('pruebaKO123');
  cy.get('#repeatPassword').type('pruebaKO123');
  cy.get('.HmiKf').click();
})
it('Ok flow', () => {

  cy.visit('');
  cy.get('.sc-kDDrLX').click();
  cy.get('.HmiKf').click();
  cy.get('#password').type('pruebaOK123');
  cy.get('#repeatPassword').type('pruebaOK123');
  cy.get('.HmiKf').click();
})