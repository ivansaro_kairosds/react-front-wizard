import { useState } from 'react'


export const useForm = ( initialState = {}, validateForm ) => {
    
    const [values, setValues] = useState(initialState);
    const [errors, setErrors] = useState({})

    const handleInputChange = ({ target }) => {
      const {id, value} = target;
      setValues({
        ...values,
        [ id ]: value
      });
      setErrors(validateForm(values))
    }

   
    return [ values, handleInputChange, errors, setValues ];
}