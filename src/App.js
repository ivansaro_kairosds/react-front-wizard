import React, { useState } from "react"
import { useTranslation } from "react-i18next"
import { Route, Routes } from 'react-router-dom'
import "./App.css"
import { Layout } from "./layout/Layout"
import { ResponseLayout } from "./layout/ResponseLayout.jsx"
import { InformationView } from "./views/InformationView"
import { PasswordView } from "./views/PasswordView"
import { ResponseErrorView } from "./views/ResponseErrorView"
import { ResponseSuccessView } from "./views/ResponseSuccessView"

function App() {

    const { i18n, t } = useTranslation('translation');
    
    const changeLanguage = (language) => {
        i18n.changeLanguage(language)
    }

    const [page, setpage] = useState('')

    return(
      <Routes>
            <Route 
                path="/" 
                element={
                    <Layout 
                        title={t('common.title')} 
                        changeLanguage={changeLanguage}
                        page={page}
                    />
                }
            >
                <Route 
                    index 
                    element={
                        <InformationView 
                            setpage={setpage}
                        />
                    }
                />
                <Route 
                    path="/newPassword" 
                    element={
                        <PasswordView
                            setpage={setpage}
                        />
                    } 
                />
            </Route>
            <Route
                element={
                    <ResponseLayout />
                }
            >
                <Route 
                    path="/success"
                    element={
                        <ResponseSuccessView 
                            title={t('successView.title')}
                            text={t('successView.text')}
                            textButton={t('successView.button')}
                        />
                    }
                />    
                <Route 
                    path="/error"
                    element={
                        <ResponseErrorView 
                            title={t('errorView.title')}
                            text={t('errorView.text')}
                            textButton={t('errorView.button')}
                        />
                    }
                />    
            </Route>    
      </Routes>
    );
}

export default App;

