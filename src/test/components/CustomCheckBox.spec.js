import '@testing-library/jest-dom'
import { fireEvent, render, screen } from '@testing-library/react'
import * as React from 'react'
import { BrowserRouter } from 'react-router-dom'
import { Checkbox } from '../../components/CustomCheckBox'


describe('CustomCheckBox component', () => {

  test('Should render component', () => {

    const label = 'Acepto'

    render(
      <BrowserRouter>
        <Checkbox label={label} />
      </BrowserRouter>
    )

    const screenText = screen.getByText(/acepto/i);
    expect(screenText).toBeInTheDocument();
  })
  
  test('Should be checked & unchecked depending on click', () => {

    const label = 'Acepto'
    const setDisabled = jest.fn().mockImplementation()

    render(
      <BrowserRouter>
        <Checkbox label={label} setDisabled={setDisabled} />
      </BrowserRouter>
    )

    
    const checkbox = screen.getByRole('checkbox');
    
    fireEvent.click(checkbox);
    expect(checkbox).toBeChecked();
    
    fireEvent.click(checkbox);
    expect(checkbox).not.toBeChecked();
  })
})