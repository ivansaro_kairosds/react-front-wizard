import '@testing-library/jest-dom'
import { render, screen } from '@testing-library/react'
import React from 'react'
import { BrowserRouter } from 'react-router-dom'
import { Path } from '../../components/Path'


describe('Path component', () => {

  test('Should render component with the prop first', () => {

    const numberScreen = 'first';
    
    render(
      <BrowserRouter>
        <Path numberScreen={numberScreen} />
      </BrowserRouter>
    )

    const number = screen.getByText(/1/i);
    expect(number).toBeInTheDocument();
    const li = screen.getByRole('list');
    expect(li.firstChild).toHaveStyle('background-color: #002B45')
  })
  
  test('Should render component with the prop second', () => {

    const numberScreen = 'second';
    
    render(
      <BrowserRouter>
        <Path numberScreen={numberScreen} />
      </BrowserRouter>
    )

    const symbol = screen.getByText(/✔/i);
    expect(symbol).toBeInTheDocument();
    const li = screen.getByRole('list');
    expect(li.firstChild).not.toHaveStyle('background-color: #002B45')

  })
})