import '@testing-library/jest-dom'
import { render, screen } from '@testing-library/react'
import React from 'react'
import { BrowserRouter } from 'react-router-dom'
import head from '../../assets/img/group.svg'
import { Card } from '../../components/Card'


describe('Card component', () => {

  test('Should render component', () => {

    const text = 'It was the best of times, it was the worst of times, it was the age of wisdom, it was the age of foolishness, it was the epoch of belief, it was the epoch of incredulity, it was the season of Light, it was the season of Darkness, it was the spring of hope, it was the winter of despair'
    
    render(
      <BrowserRouter>
        <Card text={text} img={head} />
      </BrowserRouter>
    )

    const screenText = screen.getByText(/it was the best of times/i);
    expect(screenText).toBeInTheDocument();
    const img = screen.getByRole('img');
    expect(img).toHaveAttribute('src', 'group.svg')
    expect(img).toBeInTheDocument();
  })
})