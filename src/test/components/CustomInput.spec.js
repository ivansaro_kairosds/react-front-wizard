import '@testing-library/jest-dom'
import { fireEvent, render, screen } from '@testing-library/react'
import React from 'react'
import { BrowserRouter } from 'react-router-dom'
import { CustomInput } from '../../components/CustomInput'



describe('Input component', () => {

  test('Should render component with label', () => {

    const labelProp = 'Introduce nombre'
    
    render(
      <BrowserRouter>
        <CustomInput label={labelProp}/>
      </BrowserRouter>
    )

    const label = screen.getByText(/nombre/i);
    expect(label).toBeInTheDocument();
  })

  test('Should render component with count in case of hint propr', () => {

    const labelProp = 'Introduce nombre'
    
    render(
      <BrowserRouter>
        <CustomInput label={labelProp} use='hint'/>
      </BrowserRouter>
    )
    const label = screen.getByText(/255/i);
    expect(label).toBeInTheDocument();
   
  })
  
  test('Should render component with eye in case of password prop', () => {

    const labelProp = 'Introduce nombre'
    
    render(
      <BrowserRouter>
        <CustomInput label={labelProp} use='password'/>
      </BrowserRouter>
    )
    const eye = screen.getByTestId('eye');
    expect(eye).toBeInTheDocument();
  })
  
  test('Should not show password on init but it must show it when the eye is pressed', () => {

    const labelProp = 'Introduce nombre'
    
    render(
      <BrowserRouter>
        <CustomInput label={labelProp} use='password'/>
      </BrowserRouter>
    )
    
    
    const input = screen.getByLabelText('Introduce nombre');
    fireEvent.change(input, { target: { value: '123' } });
    expect(input).toBeInTheDocument();
    const inputText = screen.queryByText('123');
    expect(inputText).not.toBeInTheDocument();
  })
})