import '@testing-library/jest-dom'
import { render, screen } from '@testing-library/react'
import React from 'react'
import { BrowserRouter } from 'react-router-dom'
import { CustomButton } from '../../components/CustomButton'

describe('CustomButton component', () => {

  test('Should render component', () => {

    const text = 'Next'
    
    render(
      <BrowserRouter>
        <CustomButton text={text} />
      </BrowserRouter>
    )

    const screenText = screen.getByText(/next/i);
    expect(screenText).toBeInTheDocument();

  })

  test('Should render the type next', () => {

    const text = 'Next'

    const { container } = render(
      <BrowserRouter>
        <CustomButton text={text} type="next"/>
      </BrowserRouter>
    )

    expect(container.firstChild).toHaveStyle('background-color: #002B45')
    expect(container.firstChild).toHaveStyle('color: white')
    expect(container.firstChild).toHaveStyle('width: 10rem')
  })
  
  test('Should render the type cancel', () => {

    const text = 'Cancel'

    const { container } = render(
      <BrowserRouter>
        <CustomButton text={text} type="cancel"/>
      </BrowserRouter>
    )

    expect(container.firstChild).toHaveStyle('background-color: transparent')
    expect(container.firstChild).toHaveStyle('color: #002B45')
    expect(container.firstChild).toHaveStyle('border: none')
  })
  
  test('Should render the type link', () => {

    const text = 'link'

    const { container } = render(
      <BrowserRouter>
        <CustomButton text={text} type="cancel"/>
      </BrowserRouter>
    )

    expect(container.firstChild).toHaveStyle('background-color: transparent')
    expect(container.firstChild).toHaveStyle('color: rgb(0, 43, 69)')
    expect(container.firstChild).toHaveStyle('border: none')
  })
})