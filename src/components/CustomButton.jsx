import React from 'react'
import { useNavigate } from "react-router-dom"
import styled, { css } from 'styled-components'


export const CustomButton = ({type, goTo, disabled, text, onSubmit,loading}) => {
  
  const navigate = useNavigate();

  return (
    <Button
      onClick={(goTo) ? () => navigate(goTo) : null}
      onSubmit={onSubmit}
      disabled={disabled}
      type={type}
    >
      {(loading) ? 'Loading...' : text}
    </Button>
  )
}

export const Button = styled.button`
border: none;
border-radius: 2px;
cursor: pointer;
font-size: 1em;
font-weight: 900;


${(props) => {
  switch (props.type) {
    case "next":
      return css`
        background-color: #002B45;
        color: white;
        height: 3rem;
        width: 10rem;
      `;
    case "cancel":
      return css`
        background-color: transparent;
        border:none;
        color: #002B45;
      `;
    case "link":
      return css`
        background-color: transparent;
        border:none;
        color: red;
      `;
    default:
      return css`
        background-color: #002B45;
        color: white;
        height: 3rem;
        width: 10rem;
      `;
  }
}}

&:disabled {
  color: grey;
  opacity: 0.7;
  cursor: not-allowed;
}

&:hover {
  opacity: .7;
}
`
