import { faEye, faEyeSlash } from "@fortawesome/free-solid-svg-icons"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import React, { useState } from "react"
import styled, { css } from "styled-components"

const eye = <FontAwesomeIcon icon={faEye} />;
const eyeSlash = <FontAwesomeIcon icon={faEyeSlash} />


export const CustomInput = ({label, use = 'password', maxLength, handleInputChange, count, id, value,}) => {
  
  const [passwordShown, setPasswordShown] = useState(false);

  const togglePasswordVisiblity = () => {
    setPasswordShown(passwordShown ? false : true);
  };
  return (
   <>
      <Label>
        {label}
        <Input 
          autocomplete="on"
          id={id} 
          value={value}
          type={use === 'password' && !passwordShown ? 
            'password' : 
            'text'
          } 
          maxLength={maxLength} 
          onChange={handleInputChange} 
          onKeyUp={handleInputChange}
        />
        { use === 'password' && 
          <Eye 
            data-testid="eye"
            onClick={togglePasswordVisiblity}
          >
            {!passwordShown ? eye : eyeSlash}
          </Eye>
        }
      </Label>
      {use === 'hint' && 
        <Count>
          {count}/255
        </Count>}
   </>
  )
}


const Label = styled.label`
  cursor: pointer;
  font-weight: bold;
  position: relative;
  width: 100%;
`;

const Input = styled.input`
  color: black;
  font-size: 1em;
  height: 3em;
  margin: 0 auto;
  margin-bottom: 14px;
  margin-top: 1em;
  opacity: 50%;
  width: 100%;
  padding: .3em 0;
  padding-left: 1em;
  
  
  ${(props) => {
    switch (props.use) {
      case 'password':
        return css`
        width: 90%;
        `;
      default:
        return css`
        width: 100%;
        `;
    }
  }}
`;

const Eye = styled.i`
  cursor: pointer;
  position: absolute;
  right: 3%;
  top: 50%;
  margin-right: .3em;
`

const Count = styled.span`
margin-left: auto;  

`
