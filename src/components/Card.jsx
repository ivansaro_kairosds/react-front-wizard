import React from 'react'
import styled from 'styled-components'


export const Card = ({img, text}) => {
  return (
    <CardContainer>
      <img src={img} alt="" />
      <div>
        {text}
      </div>
    </CardContainer>
  )
}

export const CardContainer = styled.div`
align-items: center;
display: flex;
flex-direction: column;
padding: 1rem 0;
text-align: center;
width: 50%;

& > img {
  margin-bottom: 1rem;
  width: 50%;
  height: 20rem;
}

& > div {
  max-width: 60%;
  word-wrap: break-word
}
`
