import React from 'react'
import { useTranslation } from 'react-i18next'
import styled from 'styled-components'
import { CustomButton } from './CustomButton'

export const Footer = ({goBack, goNext, backDisabled, onSubmit, values, loading, disabled})  => {
  
  const { t } = useTranslation('translation');
 
  return (
    <Foot>
      <CustomButton 
        disabled={backDisabled} 
        goTo={goBack} 
        text={t('common.buttonBack')} 
        type='cancel' 
      />
      <CustomButton 
        disabled={disabled} 
        goTo={goNext} 
        id='nextButton' 
        onSubmit={onSubmit}
        text={t('common.buttonNext')} 
        values={values}
        loading={loading}
      />
    </Foot>
  )
}

export const Foot = styled.div`
align-items: center;
bottom: 0;
display: flex;
height: 6rem;
justify-content: space-between;
`
