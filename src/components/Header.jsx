import React from 'react'
import styled from 'styled-components'
import { Path } from './Path'

export const Header = ({changeLanguage, page}) => {
  return (
    <Head>
      <Path numberScreen={page}/>
      <Language>
        <button
          border='none'
          onClick={() =>changeLanguage('es')}
        >
          ES
        </button>
        /
        <button
          border='none'
          onClick={() =>changeLanguage('en')}
        >
          EN
        </button>
      </Language>
    </Head>
  )
}

const Head = styled.div`
align-items: center;
background-color: lightGrey;
display: flex;
height: 5rem;
justify-content: center;
`

const Language = styled.div`
position: relative;
left: 25%;
color: black;
& > button {
  background-color: transparent;
  border: none;
  cursor: pointer;
}
`
