import React from 'react'
import styled, { css } from 'styled-components'

export const Path = ({numberScreen}) => {

  let firstDotText = '';

  (numberScreen === 'second') ? firstDotText = '✔' : firstDotText = '1'
  

  return (
    <DotList 
      numberScreen={numberScreen}
    >
        <li>{firstDotText}</li>
        <li>2</li>
        <li >3</li>
    </DotList>
  )
}

export const DotList = styled.ul`
font-size: 1.5rem;
z-index: 9;

& > li {
  width: 3rem;
  height: 3rem;
  text-align: center;
  line-height: 3rem;
  border-radius: 999px;
  background: darkgrey;
  margin: 0 1rem;
  display: inline-block;
  color: white;
  position: relative;
}

& > li::before {
  content: '';
  position: absolute;
  top: 1.5rem;
  left: -4rem;
  width: 4rem;
  height: .2rem;
  background: darkgrey;
  z-index: -1;
}


& > li:first-child::before {
  display: none;
}

${(props) => {
  switch (props.numberScreen) {
    case "first":
      return css`
      & > li:first-child {
        background-color: #002B45;
      }
      `;
    case "second":
      return css`
      & > li:first-child {
        background-color: red;
      }
      
      & > li:nth-child(2) {
        background-color: #002B45;
      }

      & > li:nth-child(2)::before {
        background-color: red;
      }
      `;
    default:
      return css`
      & > li:first-child {
        background-color: #002B45;
      }
      `
  }
}}
`