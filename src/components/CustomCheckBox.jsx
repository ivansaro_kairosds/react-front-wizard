import React from "react"
import styled, { keyframes } from "styled-components"

export const Checkbox = ({
  checked,
  id = 'checkbox',
  label,
  setDisabled
}) => (
  <Label htmlFor={id}>
    {label}
    <Input
      id={id}
      type='checkbox'
      checked={checked}
      onChange={() => setDisabled((disabled) => !disabled)} />
    <Indicator />
  </Label>
)

const Input = styled.input`
  height: 0;
  opacity: 0;
  width: 0;
  z-index: -1;
`;

const Label = styled.label`
  cursor: pointer;
  display: inline-block;
  margin: 0.6em 0;
  position: relative;
`;

const rotate = keyframes`
 from {
    opacity: 0;
    transform: rotate(0deg);
  }
  to {
    opacity: 1;
    transform: rotate(45deg);
  }
`;

const Indicator = styled.div`
background: #e6e6e6;
border: 1px solid #757575;
border-radius: 0.2em;
height: 1.2em;
left: -1.6em;
position: absolute;
top: 0em;
width: 1.2em;

  ${Input}:checked & {
    background: #d1d1d1;
  }

  ${Label}:hover & {
    background: #ccc;
  }

  &::after {
    content: "";
    display: none;
    position: absolute;
  }

  ${Input}:checked + &::after {
    animation-duration: 0.3s;
    animation-fill-mode: forwards;
    animation-name: ${rotate};
    border: solid #00008B;
    border-width: 0 0.2em 0.2em 0;
    display: block;
    height: 70%;
    left: 0.35em;
    top: 0.1em;
    width: 35%;
  }
`;


