import React from 'react'
import styled from 'styled-components'
import cross from '../assets/img/cross.svg'
import { CustomButton } from '../components/CustomButton'

export const ResponseErrorView = ({text, title, textButton}) => {
  return (
    <>
      <Container>
        <Icon src={cross} />
        <TextContainer>
          <Title>{title}</Title>
          <Text>{text}</Text>
        </TextContainer>
      </Container>
      <ButtonContainer>
        <CustomButton type='link' goTo={'/newPassword'} text={textButton} />
      </ButtonContainer>
    </>
  )
}

const Container = styled.div`
display: flex;
width: 100%
margin-top: 2rem;
`

const Icon = styled.img`
width: 10%;
`

const TextContainer = styled.div`
margin-left: 2rem;
`


const Title = styled.div`
  font-size: 1.2em;
  font-weight: bold;
  margin-bottom: 1rem;
`

const Text = styled.div`
`

const ButtonContainer = styled.div`
  align-items: center;
  bottom: 0;
  display: flex;
  height: 6rem;
  justify-content: flex-end;
`