import React, { useEffect, useState } from 'react'
import { useTranslation } from 'react-i18next'
import styled from 'styled-components'
import safe from '../assets/img/group-3.svg'
import head from '../assets/img/group.svg'
import { Card } from '../components/Card'
import { Checkbox } from '../components/CustomCheckBox'
import { Footer } from '../components/Footer'

export const InformationView = ({setpage}) => {
  
  useEffect(() => {
    setpage('first');
  }, [setpage])
  
  const { t } = useTranslation('translation');
  const [disabled, setDisabled] = useState(true)
  
  return (
    <>
      <CardsContainer>
        <Card img={head} text={t('informationView.headP')} />
        <Card img={safe} text={t('informationView.safeP')} />
      </CardsContainer>
      <InfoContainer>
        <h3>{t('informationView.title1')}</h3>
        <p>{t('informationView.paragraph1')}</p>
        <h3>{t('informationView.title2')}</h3>
        <p>{t('informationView.paragraph2')}</p>
       
      </InfoContainer>
      <CheckboxContainer>
        <Checkbox label={t('informationView.checkboxLabel')} setDisabled={setDisabled} />
      </CheckboxContainer>
      <Footer  disabled={disabled} backDisabled goNext='/newPassword' />
    </>
  )
}

export const CardsContainer = styled.div`
  display: flex;
`

export const InfoContainer = styled.section`
margin-top: 2rem;

& > article {
  margin-bottom: 2rem;
}

& > h3 {
  font-weight: 900;
}
`

export const CheckboxContainer = styled.section`
margin-left: 1.6em;
`
