import React, { useEffect, useState } from 'react'
import { useTranslation } from 'react-i18next'
import { useNavigate } from "react-router-dom"
import styled from 'styled-components'
import { CustomInput } from '../components/CustomInput'
import { Footer } from '../components/Footer'
import { useForm } from "../hooks/useForm"
import { submitForm } from '../services/api'


export const PasswordView = ({setpage}) => {

  useEffect(() => {
    setpage('second');
  }, [setpage])

  const navigate = useNavigate();

  const [disabled, setDisabled] = useState(true)

  const [loading, setLoading] = useState(false);
    
  const { t } = useTranslation('translation');
  
  const intitalForm = {
    password: '',
    repeatPassword: '',
    hint: ''
  }
  
  const validateForm = (formValues) => {
    const regexPassword = /^(?=.{8,})(?=.*[a-z])(?=.*[A-Z])(?=(?:\D*\d){1}).*$/;
    let errors = {};
    
    
    if(!formValues.password.trim()) {
      errors.password = 'El campo es obligatorio';
    } else if (formValues.password.trim().length < 8) {
      errors.password = 'El password ha de tener asl menos 8 caracteres';
    } else if (!regexPassword.test(formValues.password.trim())) {
      errors.password = 'El password ha de contener 1 mayúscula y 1 número'
    } else {
      errors.password = ''
    }

    if(formValues.repeatPassword.trim() !== formValues.password.trim()){
      errors.repeatPassword = 'Ambas contraseñas han de coincidir';
    } else {
      errors.repeatPassword = ''
    }

    if(!errors.password && !errors.repeatPassword){ 
      setDisabled(false)
    }

    return errors;
  }
  
  const [ formValues, handleInputChange, errors,] = useForm(intitalForm, validateForm);
  const{password, repeatPassword, hint} = formValues;
  
  const sendData = async (e) => {
    e.preventDefault();
    try {
      setLoading(true)
      await submitForm(password, repeatPassword, hint);

      navigate('/success');
      setLoading(false)
      
    } catch (err) {
      console.log(err);
      navigate('/error');
      setLoading(false)
    }
  }
  return (
    <>
      <P>{t('passwordView.explanation')}</P>
      <Form onSubmit={sendData}>
        <InputsContainer>
          <InputErrorContainer>
              <CustomInput 
                label={t('passwordView.inputLabel1')} 
                maxLength='24' 
                id={'password'} 
                handleInputChange={handleInputChange}
                value={formValues.password}
                required
              />
              {errors.password && <Error>{errors.password}</Error>}
          </InputErrorContainer>
          <InputErrorContainer>
              <CustomInput 
                label={t('passwordView.inputLabel2')} 
                maxLength='24' 
                id={'repeatPassword'} 
                handleInputChange={handleInputChange}
                value={formValues.repeatPassword}
                required
              />
                {errors.repeatPassword && <Error>{errors.repeatPassword}</Error>}
          </InputErrorContainer>
        </InputsContainer>
        <P>{t('passwordView.hintExplanation')} </P>
        <InputAnswerContainer>
          <CustomInput 
          id='hint' 
          count={formValues.hint.length}
          label={t('passwordView.inputLabel3')} 
          use='hint' 
          maxLength='255' 
          handleInputChange={handleInputChange} 
          value={formValues.hint}
        />
        </InputAnswerContainer>
      <Footer 
        errors={errors} 
        goBack='/' 
        disabled={disabled} 
        onSubmit={sendData} 
        loading={loading}
      />
      </Form>
    </>
  )
}

const P = styled.p`
width: 90%;
font-size: 1.2em;
`
const Form = styled.form``

const InputsContainer = styled.div`
display: flex;
justify-content: space-between;
width: 70%;
gap: 2rem;
`
const InputAnswerContainer = styled.div`
display: flex;
flex-direction: column;
margin-bottom: 3rem;
width: 100%;
`

const InputErrorContainer = styled.div`
display: flex;
flex-direction: column;
`

const Error = styled.span`
color: red;
font-size: .9em;
display: inline;
`
