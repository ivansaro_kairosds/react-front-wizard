export const paths = {
  first: {
    goBack: '',
    goNext: '/newPassword'
  },
  second: {
    goBack: '/',
    goNextOk: '',
    goNextKo: ''
  },
  thirdError: {
    goNext: '/',
  },
  thirdSuccess: {
    goNext: ''
  }
}