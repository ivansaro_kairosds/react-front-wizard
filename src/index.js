import i18next from 'i18next'
import React from 'react'
import { createRoot } from 'react-dom/client'
import { I18nextProvider } from 'react-i18next'
import { BrowserRouter } from 'react-router-dom'
import App from './App.js'
import './index.css'
import translation_en from './locale/en/translation.json'
import translation_es from './locale/es/translation.json'


const container = document.getElementById('root');
const root = createRoot(container);


i18next.init({
  interpolation: {
    escapeValue: false
  },
  lng: 'es',
  resources: {
    es: {
      translation: translation_es
    },
    en: {
      translation: translation_en
    },
  }
})
root.render(
  <I18nextProvider i18n={i18next}>
    <BrowserRouter>
      <App />
    </BrowserRouter>
  </I18nextProvider>
);