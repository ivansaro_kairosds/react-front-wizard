import React from 'react'
import { Outlet } from 'react-router-dom'
import styled from 'styled-components'


          
  export const ResponseLayout = () => {
  
  return (
     <LayoutStyled>
        <Body>
         <Outlet />
        </Body>
     </LayoutStyled>
  )
}

const Body = styled.div`
  padding: 6% 10%;
`

const LayoutStyled = styled.div`
  box-shadow: 5px 5px 15px 5px #000000;
  margin-left: auto;
  margin-right: auto;
  margin-top: 2rem;
  width: 50%;
  -webkit-box-shadow: 5px 5px 15px 5px #000000;
`

