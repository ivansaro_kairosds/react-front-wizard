import React from 'react'
import { Outlet } from 'react-router-dom'
import styled from 'styled-components'
import { Header } from '../components/Header'

export const Layout = ({title, changeLanguage, page}) => {
  
  return (
     <LayoutStyled>
        <Header changeLanguage={changeLanguage} page={page} />
        <Body>
          <Title >
            {title}
          </Title>
          <Outlet />
        </Body>
     </LayoutStyled>
  )
}

const Body = styled.div`
  padding: 3% 10%;
`

const Title = styled.div`
  font-size: 2.5em;
  font-weight: bold;
  margin-bottom: 2rem;
`

const LayoutStyled = styled.div`
box-shadow: 5px 5px 15px 5px #000000;
  margin-left: auto;
  margin-right: auto;
  margin-top: 2rem;
  width: 50%;
  -webkit-box-shadow: 5px 5px 15px 5px #000000;
`

